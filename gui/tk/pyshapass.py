#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
PyShaPass is a password generator wich use SHA-1 and Base64 and the
location of your protected data (web site, mailing list, compressed
file with password, etc.) for generate a password.

Copyright (c) 2004 Cedric L'HOMME
Licensed under the GNU GPL. For full terms see the file COPYING.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

version = "0.1"

from optparse import OptionParser
import user
import PyShaPass.pyshapass

from Tkinter import *
import tkMessageBox
import tkSimpleDialog 
import tkFileDialog

usage = """%prog [options]"""+"\n\n"+PyShaPass.pyshapass.intro+"\n\n"+PyShaPass.pyshapass.secure

parser = OptionParser(usage=usage, version="%prog "+version)
parser.add_option("-v","--verbose",
        action="store_true", dest="verbose", default=False, help="what's happens inside ?")
parser.add_option("-l","--length",
        action="store", dest="length", default=8, help="length of the generate password (default=8)")
parser.add_option("-p","--no-password",
        action="store_false", dest="use_password", default=True, help="do not ask a password (are you sure ?)")
parser.add_option("-e","--extrakey",
        action="store_true", dest="use_extrakey", default=False, help="use an extrakey")
parser.add_option("-x","--extrakey-file",
        action="store", type="string", dest="extrakey", metavar="FILE", help="use FILE for extrakey")
parser.add_option("-m","--mixer",
        action="store_true", dest="use_mixer", default=False, help="use mixer instead of base64")
parser.add_option("-u","--no-uppercase",
        action="store_false", dest="use_uppercase", default=True, help="in mixer : no uppercase")
parser.add_option("-n","--no-number",
        action="store_false", dest="use_number", default=True, help="in mixer : no number")
parser.add_option("-s","--no-special",
        action="store_false", dest="use_special", default=True, help="in mixer : no special character")
(options, args) = parser.parse_args()

class PyShaPass_gui:
    def __init__(self, parent, options, args):
        """Make the gui
        """
        self.options = options
        self.ent = Entry(parent,width=35)
        self.ent.config(cursor="box_spiral")
        self.ent.insert(0,"Enter a location")
        self.ent.bind('<Return>',self.EntryReturn)
        self.ent.bind('<Button-1>',self.EntryEnter)
        self.ent.pack(expand=YES)

        # Get the extrakey if needed
        if options.use_extrakey:
            if options.extrakey is None:
                self.extrakey = tkFileDialog.askopenfilename(title="PyShaPass - Select your extra key",initialdir=user.home)
            else:
                self.extrakey = options.extrakey
            if self.extrakey == ():
                tkMessageBox.showwarning("PyShaPass","It's less secure without extra key !")
                self.extrakey = ""
            else:
                try:
                    fextrakey = file(self.extrakey, "rb")
                    self.extrakey = fextrakey.read()
                except:
                    tkMessageBox.showwarning("PyShaPass","Cannot open the extrakey !\n\nContinue _WITHOUT_ extrakey !")
        else:
            self.extrakey = ""

        # Get the password if needed
        if options.use_password:
            self.password = tkSimpleDialog.askstring("PyShaPass", "Enter your password")
            if self.password == None or self.password == "":
                tkMessageBox.showwarning("PyShaPass","You didn't enter password.\n\n  It's _NOT_ secure !")
                self.password = ""
        else:
            self.password = ""

        self.ent.selection_range(0, END)
        self.ent.focus_set()

    def EntryReturn(self, event):
        """Calculate the password
        """
        entry = self.ent.get()
        self.ent.delete(0,END)
        self.ent.insert(0,"Enter a location and hit Return")
        tkMessageBox.showinfo("PyShaPass",PyShaPass.pyshapass.compute_password(entry, self.password, self.extrakey, self.options))

    def EntryEnter(self, event):
        """Clean the input when the user click.
        """
        self.ent.delete(0,END)

def main():
    """Main program
    """

    root = Tk()
    root.title("PyShaPass")
    myApp = PyShaPass_gui(root, options, args)
    root.mainloop()

if __name__ == '__main__':
    main()
