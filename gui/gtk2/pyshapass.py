#!/usr/bin/python

import sys

import pygtk
pygtk.require('2.0')

import gtk, gtk.glade

try:
    import PyShaPass.pyshapass
except:
    try:
        # try to add the path of the module if it's test instead of be in site-packages
        sys.path.insert(0, "../..")
        import PyShaPass.pyshapass
    except:
        print "Check your install, I can find the PyShaPass module."
        sys.exit(-1)

class App:
    """
    The main window
    """
    def __init__(self):
        """
        Initialise the application
        """
        xml = gtk.glade.XML('./pyshapass.glade')
        xml.signal_autoconnect(self)
        self.xml = xml
        self.options = {"scrambler":0,
                        "password_size":8,
                        "scrambler_internal_uppercase":1,
                        "scrambler_internal_number":1,
                        "scrambler_internal_special":0,
                        }

    def __getitem__(self, key):
        """
        Attach all controls like in a dict
        """
        return self.xml.get_widget(key)

    def on_btn_exit_clicked(self, button):
        """
        Exit the application
        """
        sys.exit()

    def on_btn_hash_clicked(self, button):
        """
        Get everything and hash that thing
        """
        self["lbl_result"].set_text(PyShaPass.pyshapass.compute_password(self["ent_location"].get_text(),
                                                    password = self["ent_password"].get_text(),
                                                    length = self.options["password_size"],
                                                    mixer = self.options["scrambler"],
                                                    uppercase = self.options["scrambler_internal_uppercase"],
                                                    number = self.options["scrambler_internal_number"],
                                                    special = self.options["scrambler_internal_special"],
                                                    ))
        self["wdw_result"].show_all()
        self["btn_ok"].grab_focus()
    
    def on_btn_options_clicked(self, button):
        """
        View options
        """
        self["wdw_options"].show_all()
        self["btn_validate"].grab_focus()

    # View screen

    def on_btn_ok_clicked(self, button):
        """
        Exit the application
        """
        self["wdw_result"].hide_all()

    # Options screen

    def on_btn_validate_clicked(self, button):
        """
        Close options
        """
        self["wdw_options"].hide_all()

app = App()
gtk.main()
