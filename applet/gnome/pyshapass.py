#!/usr/bin/env python

import sys
import pygtk
pygtk.require('2.0')
import gtk
import gtk.glade
import gnomeapplet
import gnome.ui
import os.path
import re
import gobject
import pyshapass_globals as pglobals
            
def pyshapass_factory(applet, iid):
    label = gtk.Label("PyShaPass")
    applet.add(label)

    applet.show_all()
    return True

if len(sys.argv) == 2 and sys.argv[1] == "run-in-window":   
    main_window = gtk.Window(gtk.WINDOW_TOPLEVEL)
    main_window.set_title("PyShaPass")
    main_window.connect("destroy", gtk.main_quit) 
    app = gnomeapplet.Applet()
    pyshapass_factory(app, None)
    app.reparent(main_window)
    main_window.show_all()
    gtk.main()
    sys.exit()

gnomeapplet.bonobo_factory("OAFIID:GNOME_PyShaPass_Factory", 
                             gnomeapplet.Applet.__gtype__, 
                             "PyShaPass hello !", "0", pyshapass_factory)
    
