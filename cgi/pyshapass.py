#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
PyShaPass is a password generator wich use SHA-1 and Base64 and the
location of your protected data (web site, mailing list, compressed
file with password, etc.) for generate a password.

Copyright (c) 2004 Cedric L'HOMME
Licensed under the GNU GPL. For full terms see the file COPYING.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import sys
sys.path.insert(0, "..")

import PyShaPass.pyshapass

import cgi
import base64
import os.path

print "Content-Type: text/html\n\n"

if os.path.exists("footer.html"):
    f = open("footer.html","r")
    footer = f.read()
    f.close()
else:
    footer = ""

head = """<head>
    <title>PyShaPass</title>
    <style type="text/css">
body {
    background-color: #000000;
    color: lime;
    font-size: small;
    }
.container {
    width: 360px;
    }
.intro p{
    padding: 5px;
    }
.form_input input{
    border:1px solid lime;
    background-color: black;
    color: lime;
    }
.form_input input:hover {
    background-color: #333333;
    }
.form_input input[type=text], input[type=password] {
    width: 200px;
    padding-left: 2px;
    position: absolute;
    left: 150px;
    }
.form_input input[type=submit] {
    width: 100px;
    position: absolute;
    left: 250px;
    margin-top: 10px;
    }
p.password {
    color: #007700;
    }
    </style> 
</head>"""

def generate_form():
    print """<html>
    %s
<body>
<div class="container">
    <div class="intro">
        <p>%s</p>
    </div>
    <div class="form_input">
    <form method="post" action="pyshapass.py" name="form_psp">
    <p>Location<input type="text" name="location"></p>
    <p>Password<input type="password" name="password"></p>
    <input type="hidden" name="action" value ="display">
    <input type="submit" value="Enter">
    </form>
    <script type="text/javascript">document.form_psp.location.focus();</script>
    </div>
</div>
%s
</body>
</html>""" % (head, PyShaPass.pyshapass.intro, footer)

def display_data(password):
    print """<html>
    %s
<body>
<div class="container">
    <div class="intro">
        <p class="password">%s</p>
    </div>
    <div class="form_input">
    <form method="post" action="pyshapass.py" name="form_retry">
        <input type="submit" value="Again !" name="button_retry">
    </form>
    </div>
</div>
%s
</body>
</html>
""" % (head, password, footer)

def main():
    form = cgi.FieldStorage()
    if (form.has_key("action") and form.has_key("location")):
        if (form["action"].value == "display"):
            if form.has_key("password"):
                password = form["password"].value
            else:
                password = ""
            if form.has_key("location"):
                location = form["location"].value
            else:
                location = ""
            display_data(PyShaPass.pyshapass.compute_password(location, password))
    else:
        generate_form()

if __name__ == '__main__':
    main()
