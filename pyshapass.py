#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
PyShaPass is a password generator wich use SHA-1 and Base64 and the
location of your protected data (web site, mailing list, compressed
file with password, etc.) for generate a password.

Copyright (c) 2004 Cedric L'HOMME
Licensed under the GNU GPL. For full terms see the file COPYING.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

version = "0.1"

import PyShaPass.pyshapass

from optparse import OptionParser

usage = """%prog [options]"""+"\n\n"+PyShaPass.pyshapass.intro+"\n\n"+PyShaPass.pyshapass.secure

parser = OptionParser(usage=usage, version="%prog "+version)
parser.add_option("-v","--verbose",
        action="store_true", dest="verbose", default=False, help="what's happens inside ?")
parser.add_option("-l","--length",
        action="store", dest="length", default=8, help="length of the generate password (default=8)")
parser.add_option("-p","--no-password",
        action="store_false", dest="use_password", default=True, help="do not ask a password (are you sure ?)")
parser.add_option("-e","--extrakey",
        action="store_true", dest="use_extrakey", default=False, help="use an extrakey")
parser.add_option("-x","--extrakey-file",
        action="store", type="string", dest="extrakey", metavar="FILE", help="use FILE for extrakey")
parser.add_option("-m","--mixer",
        action="store_true", dest="use_mixer", default=False, help="use mixer instead of base64")
parser.add_option("-u","--no-uppercase",
        action="store_false", dest="use_uppercase", default=True, help="in mixer : no uppercase")
parser.add_option("-n","--no-number",
        action="store_false", dest="use_number", default=True, help="in mixer : no number")
parser.add_option("-s","--no-special",
        action="store_false", dest="use_special", default=True, help="in mixer : no special character")
(options, args) = parser.parse_args()

import getpass

def beware(message):
    """Print a beware
    """
    print "/!\ %s" % (message)

def main():
    """Main program
    """

    # extrakey
    if options.use_extrakey:
        if options.extrakey is None:
            path_extrakey=raw_input("Extrakey: ")
        else:
            path_extrakey=options.extrakey
        if path_extrakey==():
            beware("The extrakey can't be empty if you choose to use one.")
            return -1
        else:
            try:
                fextrakey = file(path_extrakey, "rb")
                extrakey = fextrakey.read()
                if extrakey == "":
                    beware("The extrakey can't be empty if you choose to use one.")
                    return -1
            except:
                beware("Cannot open the extrakey.")
                return -1
    else:
        print "You choose to not use an extrakey !"
        extrakey=""

    # password
    if options.use_password:
        password = getpass.getpass()
        if password == None or password == "":
            beware("The password can't be empty if you choose to use one.")
            return -1
    else:
        print "You choose to not use a password !"
        password = ""

    # location
    location=raw_input("Location: ")
    
    print PyShaPass.pyshapass.compute_password(location, password, extrakey,
            length = options.length,
            mixer = options.use_mixer,
            uppercase = options.use_uppercase,
            number = options.use_number,
            special = options.use_special,
            verbose = options.verbose)
    #~ TODO how erase the line with the password like in the bash version ?
    #~ sys.stdout.write(compute_password(extrakey, password, entry))
    #~ raw_input("\r")

if __name__ == '__main__':
    main()
