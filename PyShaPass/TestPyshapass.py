import unittest
import pyshapass

class TestPyshapass(unittest.TestCase):

    def test_nopasswd_python(self):
        self.assertTrue(pyshapass.compute_password("http://python.org", "", "")=="dTX4lqkb")

    def test_passwd_python(self):
        self.assertTrue(pyshapass.compute_password("http://python.org", "secret_pass", "")=="xcMWfIL3")

if __name__ == '__main__':
    unittest.main()