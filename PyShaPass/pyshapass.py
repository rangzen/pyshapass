#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
See the README of PyShaPass.

Copyright (c) 2004 Cedric L'HOMME
Licensed under the GNU GPL. For full terms see the file COPYING.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import base64
import hashlib

intro = """PyShaPass is a deterministic password generator wich use SHA-1, Base64
or an internal scrambler and the location of your protected data
(web site, mailing list, compressed file with password, etc.) for
generate a password."""

secure = """The 3 ways to secure your location :

- a password must be strong in case of unsecure extrakey or semi public,
it can be simple if the extrakey is well protected.

- an extrakey can be binary, random or your favorite poems (bad idea),
it must be stable and secure like your private PGP key.

- some rules to transform the location
Example : a "!" after the first letter and delete of the first letter of
the top level domain
linuxfr.org -> l!inuxfr.rg
ml@gnu.org -> m!l@gnu.rg"""

def compute_password(location, password = "", extrakey = "", length = 8, mixer = 0, uppercase = 1, number = 1, special = 0, verbose = 0):
    """Compute the password
    """
    if verbose:
        print "mixer options U=%s N=%s S=%s" % (uppercase, number, special)

    # concat everything
    alice = location + password + extrakey

    # use the internal mixer or the original way
    if mixer:
        return pyshapass_mixer(hashlib.sha1(alice).digest(), length, uppercase, number, special, verbose)
    else:
        return base64.encodestring(hashlib.sha1(alice).digest())[0:int(length)]

def pyshapass_mixer(location, length, uppercase, number, special, verbose = 0):
    """Mix the SHA with indice instead of base64
    """
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    if uppercase:
        alphabet += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    if number:
        alphabet += "0123456789"
    if special:
        alphabet += '=+-/*.!?:%$_&@"''(){}[]'

    if verbose:
        print "Alphabet=%s" % (alphabet)

    la = len(alphabet)
    password = ""
    for i in range(int(length)):
        password += alphabet[ord(location[i]) % la]
    return password

