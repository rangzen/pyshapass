#!/bin/bash

# See the README of PyShaPass.
# 
# Copyright (c) 2004 Cedric L'HOMME
# Licensed under the GNU GPL. For full terms see the file COPYING.
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

echo -n "Password: "
read -s pass
echo -ne "\r            \rLocation: "
read -s www
echo -ne "\rmdp : `echo -n $pass$www | openssl sha1 -binary | openssl base64 | cut -b 1,2,3,4,5,6,7,8` (Enter)"
read -s
echo -ne "\r                      \r"
